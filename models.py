from pymongo import MongoClient


class Weather(object):
    def __init__(self):
        client = MongoClient('localhost', 27017)
        db_weather = client.db_weather
        self.db_weather = db_weather

    def insert_document(self, json, album):
        """ Insert in collection albúm
        :param json: file content data for insert in database
        :param album: name collection
        :return:
        """
        album = self.db_weather.album
        data = album.insert_one(json)
        return data


