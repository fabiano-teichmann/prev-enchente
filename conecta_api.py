import requests
from json import loads
from models import Weather


class ClimaTempo(object):
    def __init__(self):
        self.token = '8e1d8b6826c2c35e4903bf6d6dc6641e'
        self.url_forecast = 'http://apiadvisor.climatempo.com.br/api/v1/forecast/'
        self.url_weather = 'http://apiadvisor.climatempo.com.br/api/v1/weather'
        # id city or micro region

    def get_city(self, city, state):
        """Pega id da cidade

        :param city: <str>
        :param state: <str>
        :return: cod cidade
        """
        url = '/api/v1/locale/city?name=CityName&state=StateAbbr&token=your-app-token'
        url_base = 'http://apiadvisor.climatempo.com.br/api/v1/locale/city?name={}&state={}&token={}'.format(city, state, self.token)
        response = requests.get(url_base)
        id_city = loads(response.text)
        return str(id_city[0]['id'])

    def forecast_15_day(self, locale):
        """ 15 day forecast

        :param locale: id city our micro region
        :type str:
        :return: try insert json in database
        """
        url_forecast = 'http://apiadvisor.climatempo.com.br/api/v1/forecast/locale/{}/days/15?token={}'.format(locale, self.token)
        response = requests.get(url_forecast)
        forecast = loads(response.text)
        # save_forecast = Weather().insert_document(forecast)
        return forecast

    def forecast(self, locale, period):
        """ Faz previsão do tempo agora em 72 ou 15 dias

        :param locale: <str> id cidade
        :param period: <str> periodo Now é agora ou para 15 ou 72 dias
        :return:
        """
        if period == "Now":
            request = self.url_weather + 'locale/' + locale + '/current?token=' + self.token
            url = 'http://apiadvisor.climatempo.com.br/api/v1/weather/locale/{}/current?token={}'.format(locale, self.token)
        else:
            url = 'http://apiadvisor.climatempo.com.br/api/v1/forecast/locale/{}/{}?token={}'.format(locale, period, self.token)
        response = requests.get(url)
        forecast = loads(response.text)
        return forecast


    def forecast_72_hours(self, locale):
        """ 72 hour weather forecast

        :param locale: id city our micro region
        :type str:
        :return:  try insert json in database
        """

        request = self.url_forecast + 'locale/' + locale + '/hours/72?token=' + self.token
        response = requests.get(request)
        forecast = loads(response.text)
        # save_forecast = Weather().insert_document(forecast)
        return forecast

    def forest_weather(self, locale):
        request = self.url_weather + 'locale/' + locale + '/current?token=' + self.token
        response = requests.get(request)
        weather = loads(response.text)
        return weather


if __name__ == "__main__":
    #x = ClimaTempo().get_city(city="Florianópolis", state="SC")
    print("##Bem vindo a previsão de tempo ##")
    city = str(input('Digite a cidade: '))
    state = str(input("Estado: "))
    id_city = ClimaTempo().get_city(city, state)
    tempo = ClimaTempo().forecast(locale=id_city, period="Now")
    print(tempo)



